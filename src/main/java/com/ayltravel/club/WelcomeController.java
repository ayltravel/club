package com.ayltravel.club;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WelcomeController {

	@GetMapping("/")
	public String welcomePage(final Model model) {
		model.addAttribute("title", "Club Service");
		model.addAttribute("msg", "Club Service");
		return "welcome";
	}
}
